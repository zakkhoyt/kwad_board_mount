// OpenSCAD cheat sheet: http://openscad.org/cheatsheet/
// Command line: https://files.openscad.org/documentation/manual/Using_OpenSCAD_in_a_command_line_environment.html
// Rounded cube: https://danielupshaw.com/openscad-rounded-corners/
// Arrays and iteration: https://www.robmiles.com/journal/2020/1/30/using-arrays-in-openscad


/* [Render Parameters] */
// ----------------------------------------------------------------------

// Render quality.
render_quality = "preview"; // ["fast preview", "preview", "final rendering"]

/* [Plate Parameters] */
// ----------------------------------------------------------------------

// The height of the main rect plate [mm].
plate_height = 1;

// The padding to add to the outside rect [mm]. 
outside_padding = 0;

// The padding to add to the inside rect [mm]. 
inside_padding = 1;

// The inset to remove from the inside rect [mm]. 
inside_inset = 1;

/* [Angle Parameters] */
// ----------------------------------------------------------------------

// The angle between outside and inside holes [degrees].
outside_inside_angle = 45;

/* [Hole Parameters] */
// ----------------------------------------------------------------------

// The x/y distance between the outside holes of the rect [mm]. 
outside_hole_xy = 30.0;
// outside_hole_xy = 25.5;

// The diameter of the outside mounting hole [mm].
outside_hole_diameter = 2.0;

// The thickness of the outside cylendar walls. 
outside_hole_wall_thickness = 1;

// The height of the outside mounting cylendar [mm].
outside_hole_height = 3.0;
// outside_hole_height = 0.0;


/* [Inside Hole Parameters] */

// The x/y distance between the inside holes of the rect [mm]. 
inside_hole_xy = 25.5;
// inside_hole_xy = 20.0;

// The diameter of the inside mounting hole [mm].
inside_hole_diameter = 2.0;

// The thickness of the inside cylendar walls. 
inside_hole_wall_thickness = 1;

// The height of the inside mounting cylendar [mm].
inside_hole_height = 0;
// inside_hole_height = 3.0;

/* [Hidden] */
// ----------------------------------------------------------------------

// Fragment number in a full circle. (Threads set their own number.)
$fn = (render_quality == "final rendering") ? 60 :
      (render_quality == "preview") ? 45 : 
      (render_quality == "fast preview") ? 30 : 30; // Same as the default $fa == 12.

// $fa = 1; // minimum angle
// $fs = 0.4; // minimum size
// $fn = 40; // number of fragments
// $vpr = [45, 0, 90]; // viewport rotatation

// Amount of overlap for joins and extended cuts to prevent z-fighting.
nothing = 0.01;


// The radius of the outside mounting hole [mm].
outside_hole_radius = outside_hole_diameter / 2;

inside_hole_radius = inside_hole_diameter / 2;

// Distance between the outside and inside holes. 
mount_delta = (outside_hole_xy - inside_hole_xy);

// Data for translating to the 4 corners or a rect
corners = [[0, 0], [0, 1], [1, 0], [1, 1]];


holes = [[-0.5, -0.5], [-0.5, 0.5], [0.5, -0.5], [0.5, 0.5]];

/* Shape Modules */
// ----------------------------------------------------------------------

module rounded_square(xy, corner_radius, height) {
  linear_extrude(height) {
    translate([corner_radius, corner_radius, 0]) {
      minkowski() {
        square(xy - 2 * corner_radius);
        circle(corner_radius);
      }
    }
  }
}

module cored_cynlinder(outside_radius, inside_radius, height) {
  difference() {
    cylinder(h = height, r = outside_radius);
    translate([0, 0, -nothing]){
      cylinder(h = height + 2 * nothing, r = inside_radius);
    }
  }
}

/* Helper Modules */
// ----------------------------------------------------------------------

// Creates a corred out cylinder sized for the inside holes. 
module outside_cored_cynlinder() {
  cored_cynlinder(
    outside_radius=outside_hole_radius + inside_hole_wall_thickness,
    inside_radius=inside_hole_radius,
    height=plate_height + outside_hole_height
  );
}

// Creates a corred out cylinder sized for the inside holes. 
module inside_cored_cynlinder() {
  cored_cynlinder(
    outside_radius=inside_hole_radius + inside_hole_wall_thickness,
    inside_radius=inside_hole_radius,
    height=plate_height + inside_hole_height
  );
}

module outside_cylinder() {
  cylinder(
    h = plate_height + outside_hole_height, 
    r = outside_hole_radius + outside_hole_wall_thickness
  );
}
module outside_core() {
  translate([0, 0, -nothing]){
    cylinder(
      h = plate_height + outside_hole_height + 2 * nothing, 
      r = outside_hole_radius
    );
  }
}

module inside_cylinder() {
  cylinder(
    h = plate_height + inside_hole_height, 
    r = inside_hole_radius + inside_hole_wall_thickness
  );
}
module inside_core() {
  translate([0, 0, -nothing]){
    cylinder(
      h = plate_height + inside_hole_height + 2 * nothing, 
      r = inside_hole_radius
    );
  } 
}

/* Shape Modules */
// ----------------------------------------------------------------------

module main () {
  translate([
    outside_hole_radius + outside_hole_wall_thickness,
    outside_hole_radius + outside_hole_wall_thickness, 
    inside_hole_height
  ]) {
    // Hollow out the cylendars
    difference() {
      // Append hole cylendars
      union() {
        // Prep the rect body with hollow center
        difference() {
          translate([
            outside_hole_xy / 2,
            outside_hole_xy / 2,
            0
          ]) {          // // Each cylinder solids (no hole yet))
            rotate([0, 0, 0]) {
              translate([
                -(outside_hole_xy + 2 * outside_padding) / 2,
                -(outside_hole_xy + 2 * outside_padding) / 2,
                0
              ]) {          // // Each cylinder solids (no hole yet))
                rounded_square(
                  xy = outside_hole_xy + 2 * outside_padding, 
                  corner_radius = outside_hole_radius + outside_hole_wall_thickness, 
                  height = plate_height
                );
              }
            }

            // Inside body rect (the outline of a rounded rect, extruded vertically)
            rotate([0, 0, outside_inside_angle]) {
              translate([
                -(inside_hole_xy + 2 * inside_padding) / 2,
                -(inside_hole_xy + 2 * inside_padding) / 2,
                0
              ]) {          // // Each cylinder solids (no hole yet))
                rounded_square(
                  xy = inside_hole_xy + 2 * inside_padding, 
                  corner_radius = inside_hole_radius + inside_hole_wall_thickness, 
                  height = plate_height
                );
              }
            }
          }

          // Cut outs (square)
          translate([
            outside_hole_xy / 2,
            outside_hole_xy / 2,
            0
          ]) {
            putout_xy = inside_hole_xy + (2 * inside_inset);
            translate([-(putout_xy) / 2, -putout_xy / 2, -1]) {
              // translate([-10, 0, -1]) {
              rounded_square(
                xy = inside_hole_xy + 2 * inside_inset,
                corner_radius = inside_hole_radius + inside_hole_wall_thickness, 
                height = plate_height + 2 //* nothing
              );
            }

            cutout_xy = inside_hole_xy - (inside_hole_radius + inside_hole_wall_thickness + inside_inset);
            rotate([0, 0, outside_inside_angle]) 
            translate([-cutout_xy / 2, -cutout_xy / 2, -1]) {
              rounded_square(
                xy = cutout_xy,
                corner_radius = inside_hole_radius + inside_hole_wall_thickness, 
                height = plate_height + 2 //* nothing
              );
            }
          }
        }

        // Each cylinder solid (no hole yet))
        translate([
          outside_hole_xy / 2,
          outside_hole_xy / 2,
          0
        ]) {          // // Each cylinder solids (no hole yet))
          // Each outside cylinder solids (no hole yet))
          for (i = [0: len(holes) - 1]) {
            hole = holes[i];
            translate([hole.x * outside_hole_xy, hole.y *outside_hole_xy, 0]) {
              outside_cylinder();
            }
          }

          // Each inside cylinder solids (no hole yet))
          for (i = [0: len(holes) - 1]) {
            hole = holes[i];

            // Inside
            rotate([0, 0, outside_inside_angle]) {
              translate([hole.x * inside_hole_xy, hole.y * inside_hole_xy, 0]) {
                inside_cylinder();
              }
            }
          }
        }
      }

      // Cut outs (holes)
      union() {
        translate([
          outside_hole_xy / 2,
          outside_hole_xy / 2,
          0
        ]) {
          // Outside holes
          for (i = [0: len(holes) - 1]) {
            hole = holes[i];
            // Outside
            translate([hole.x * outside_hole_xy, hole.y *outside_hole_xy, 0]) {
              outside_core();
            }
          }

          // Inside holes
          for (i = [0: len(holes) - 1]) {
            hole = holes[i];
            // // Outside
            // Inside
            rotate([0, 0, outside_inside_angle]) 
            translate([hole.x * inside_hole_xy, hole.y * inside_hole_xy, 0]) {
              inside_core();
            }
          }
        }
      }
    }
  }
}

main();