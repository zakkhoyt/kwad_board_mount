// OpenSCAD cheat sheet: http://openscad.org/cheatsheet/
// Rounded cube: https://danielupshaw.com/openscad-rounded-corners/
// Arrays and iteration: https://www.robmiles.com/journal/2020/1/30/using-arrays-in-openscad

/* [Plate Parameters] */
// ----------------------------------------------------------------------

// The height of the main rect plate [mm].
plate_height = 1;

// The outset to add to the rect [mm]. 
plate_outset = 0;

// The inset to remove from the inside of the rect [mm]. 
plate_inset = 1;


/* [Hole Parameters] */
// ----------------------------------------------------------------------

// The angle between outside and inside holes [degrees].
hole_set_angle = 45;

// The x/y distance between the outside holes of the rect [mm]. 
outside_hole_xy = 30.0;

// The diameter of the outside mounting hole [mm].
outside_hole_diameter = 3.0;

// The thickness of the outside cylendar walls. 
outside_hole_wall_thickness = 0.5;

// The height of the outside mounting cylendar [mm].
outside_hole_height = 2.0;


/* [Inside Hole Parameters] */

// The x/y distance between the inside holes of the rect [mm]. 
inside_hole_xy = 25.5;
// inside_hole_xy = 20.0;

// The diameter of the inside mounting hole [mm].
inside_hole_diameter = 2.0;

// The thickness of the inside cylendar walls. 
inside_hole_wall_thickness = 0.5;

// The height of the inside mounting cylendar [mm].
inside_hole_height = +0.0;

/* [Hidden] */
// ----------------------------------------------------------------------

// $fa = 1; // minimum angle
// $fs = 0.4; // minimum size
$fn = 40; // number of fragments
// $vpr = [45, 0, 90]; // viewport rotatation

// Amount of overlap for joins and extended cuts to prevent z-fighting.
nothing = 0.01;


// The radius of the outside mounting hole [mm].
outside_hole_radius = outside_hole_diameter / 2;

inside_hole_radius = inside_hole_diameter / 2;

// Distance between the outside and inside holes. 
mount_delta = (outside_hole_xy - inside_hole_xy);

// Data for translating to the 4 corners or a rect
corners = [[0, 0], [0, 1], [1, 0], [1, 1]];


holes = [[-0.5, -0.5], [-0.5, 0.5], [0.5, -0.5], [0.5, 0.5]];

/* Shape Modules */
// ----------------------------------------------------------------------

module rounded_square(xy, radius_corner, height) {
  linear_extrude(height) {
    translate([radius_corner, radius_corner, 0]) {
      minkowski() {
        square(xy - 2 * radius_corner);
        circle(radius_corner);
      }
    }
  }
}

module cored_cynlinder(outside_radius, inside_radius, height) {
  difference() {
    cylinder(h = height, r = outside_radius);
    translate([0, 0, -nothing]){
      cylinder(h = height + 2 * nothing, r = inside_radius);
    }
  }
}

/* Helper Modules */
// ----------------------------------------------------------------------

// Creates a corred out cylinder sized for the inside holes. 
module outside_cored_cynlinder() {
  cored_cynlinder(
    outside_radius=outside_hole_radius + inside_hole_wall_thickness,
    inside_radius=inside_hole_radius,
    height=plate_height + outside_hole_height
  );
}

// Creates a corred out cylinder sized for the inside holes. 
module inside_cored_cynlinder() {
  cored_cynlinder(
    outside_radius=inside_hole_radius + inside_hole_wall_thickness,
    inside_radius=inside_hole_radius,
    height=plate_height + inside_hole_height
  );
}

module outside_cylinder() {
  cylinder(
    h = plate_height + outside_hole_height, 
    r = outside_hole_radius + outside_hole_wall_thickness
  );
}
module outside_core() {
  translate([0, 0, -nothing]){
    cylinder(
      h = plate_height + outside_hole_height + 2 * nothing, 
      r = outside_hole_radius
    );
  }
}

module inside_cylinder() {
  cylinder(
    h = plate_height + inside_hole_height, 
    r = inside_hole_radius + inside_hole_wall_thickness
  );
}
module inside_core() {
  translate([0, 0, -nothing]){
    cylinder(
      h = plate_height + inside_hole_height + 2 * nothing, 
      r = inside_hole_radius
    );
  }
}

/* Shape Modules */
// ----------------------------------------------------------------------

module main () {
  translate([0, 0, inside_hole_height]) {
    difference() {
      union() {
        // The main body (the outline of a rounded rect, extruded vertically)
        difference() {
          translate([-plate_outset, -plate_outset, 0]) {
            rounded_square(
              xy = outside_hole_xy + 2 * plate_outset, 
              radius_corner = outside_hole_radius + outside_hole_wall_thickness, 
              height = plate_height
            );
          }
          translate([mount_delta / 2 - plate_inset, mount_delta / 2 - plate_inset, - nothing]) {
            rounded_square(
              xy = inside_hole_xy + 2 * plate_inset, 
              radius_corner = inside_hole_radius + inside_hole_wall_thickness, 
              height = plate_height + 2 * nothing
            );
          }
        }

        translate([
          outside_hole_xy / 2,
          outside_hole_xy / 2,
          0
        ]) {
          // Each cylinder solids (no hole yet))
          for (i = [0: len(holes) - 1]) {
            corner = holes[i];

            // Outside
            translate([corner.x * outside_hole_xy, corner.y *outside_hole_xy, 0]) {
              outside_cylinder();
            }

            // Inside
            rotate([0, 0, hole_set_angle]) {
              translate([corner.x * inside_hole_xy, corner.y * inside_hole_xy, 0]) {
                inside_cylinder();
              }
            }
          }
        }
      }

      translate([
        outside_hole_xy / 2,
        outside_hole_xy / 2,
        0
      ]) {
        // Each cylinder hole
        for (i = [0: len(holes) - 1]) {
          corner = holes[i];
          // Outside
          translate([corner.x * outside_hole_xy, corner.y *outside_hole_xy, 0]) {
            outside_core();
          }

          // Inside
          rotate([0, 0, hole_set_angle]) 
          translate([corner.x * inside_hole_xy, corner.y * inside_hole_xy, 0]) {
            inside_core();
          }
        }
      }
    }
  }
}

main();