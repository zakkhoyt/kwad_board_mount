# An OpenSCAD project to customize Kwad board mounts.
Examples include:
* 20mm to 30mm mount 
* 25.5mm to 30mm mount
* 25.5mm to 20mm mount

# Repository
This repository is found on [GitLab](https://gitlab.com/zakkhoyt/kwad_board_mount).