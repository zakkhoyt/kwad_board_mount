
# https://files.openscad.org/documentation/manual/Using_OpenSCAD_in_a_command_line_environment.html


# Default values
# /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD \
#     straightwall_screwtop_container.scad \
#     -o 'test_out.stl' \
#     -D 'render_quality="preview"' \
#     -D 'parts="vial & cap (side by side)"' \
#     -D 'centered="yes"' \
#     -D 'cross_section="no"' \
#     -D 'total_h=25' \
#     -D 'total_w=20' \
#     -D 'inner_dia_widen=true' \
#     -D 'cap_h_percent=30' \
#     -D 'wall_t=1.8' \
#     -D 'cap_turns=2' \
#     -D 'thread_starts=2' \
#     -D 'cap_text="NaCl"' \
#     -D 'cutter_count=15' \
#     -D 'cutter_width=2' \
#     -D 'cutter_height=-1' \
#     -D 'cutter_depth=2' \
#     -D 'hole_radius=1' \
#     -D 'hole_count=6' \
#     -D 'hole_offset=-1'





# --preview 

# /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD \
#     straightwall_screwtop_container.scad \
#     -o 'test_out_2.stl' \
#     -D 'render_quality="preview"' \
#     -D 'parts="vial & cap (side by side)"' \
#     -D 'centered="yes"' \
#     -D 'cross_section="no"' \
#     -D 'total_h=70' \
#     -D 'total_w=30' \
#     -D 'inner_dia_widen=true' \
#     -D 'cap_h_percent=20' \
#     -D 'wall_t=1.8' \
#     -D 'cap_turns=2' \
#     -D 'thread_starts=2' \
#     -D 'cap_text="NaCl"' \
#     -D 'cutter_count=15' \
#     -D 'cutter_width=2' \
#     -D 'cutter_height=-1' \
#     -D 'cutter_depth=2' \
#     -D 'hole_radius=1' \
#     -D 'hole_count=0' \
#     -D 'hole_offset=-1'


# lines=("zakk" "lindy")
lines=(44 55 66)
for line in "${lines[@]}"; do
    # Print user and # of branches: zakk [100]
    printf "line: $line\n"
done
